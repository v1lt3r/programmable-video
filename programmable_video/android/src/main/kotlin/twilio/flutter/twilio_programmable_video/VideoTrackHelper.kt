import android.content.Context
import tvi.webrtc.*
import com.twilio.video.*
import twilio.flutter.twilio_programmable_video.TwilioProgrammableVideoPlugin

class VideoTrackHelper {
    companion object {
        var rotate: Int = 0;

        fun createLocalVideoTrack(videoTrack: Map<*, *>, applicationContext: Context): LocalVideoTrack {
            rotate = videoTrack["rotate"] as Int

            if (rotate == 0) {
                return LocalVideoTrack.create(applicationContext, videoTrack["enable"] as Boolean, TwilioProgrammableVideoPlugin.cameraCapturer!!, videoTrack["name"] as String)!!
            }

            val localVideoTrackCreated = LocalVideoTrack.create(
                applicationContext,
                videoTrack["enable"] as Boolean,
                TwilioProgrammableVideoPlugin.cameraCapturer!!,
                videoTrack["name"] as String,
            )

            val originalObserver = localVideoTrackCreated!!.videoSource.capturerObserver

            var capturerObserver = object : CapturerObserver {
                private lateinit var modifiedVideoFrame: VideoFrame

                override fun onFrameCaptured(videoFrame: VideoFrame) {
                    // Modify the rotation of the video frame here
                    val originalRotation = videoFrame.rotation
                    val modifiedRotation = (originalRotation + rotate) % 360 // Rotate X degrees

                    // Create a new VideoFrame with the original buffer and the modified rotation.
                    modifiedVideoFrame = VideoFrame(videoFrame.buffer, modifiedRotation, videoFrame.timestampNs)

                    // Call the original listener's onFrameCaptured method with the modified video frame.
                    originalObserver.onFrameCaptured(modifiedVideoFrame)
                }

                // Other methods of CapturerObserver can be overridden as needed

                override fun onCapturerStarted(success: Boolean) {
                    originalObserver.onCapturerStarted(success)
                }

                override fun onCapturerStopped() {
                    originalObserver.onCapturerStopped()
                }
            }

            val eglBaseContext = EglBase.create().eglBaseContext

            val surfaceTextureHelper = SurfaceTextureHelper.create("CaptureThread", eglBaseContext)

            TwilioProgrammableVideoPlugin.cameraCapturer!!.initialize(
                surfaceTextureHelper,
                TwilioProgrammableVideoPlugin.pluginHandler.applicationContext,
                capturerObserver)

            return localVideoTrackCreated
        }
    }
}
