/// Base model that a plugin implementation can use to construct a Track.
///
/// Other track models can extend this model to implement various kinds of Tracks.
abstract class TrackModel {
  final String name;
  final bool enabled;
  final int rotate;

  const TrackModel({
    required this.name,
    required this.enabled,
    this.rotate = 0,
  });

  @override
  String toString() {
    return '{ name: $name, enabled: $enabled, rotate: $rotate }';
  }

  /// Create map from properties.
  Map<String, Object?> toMap() {
    return <String, Object?>{'enable': enabled, 'name': name, 'rotate': rotate};
  }
}
